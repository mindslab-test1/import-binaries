## What is this?
This repository run as a remote file system.
You can access any files using the following command.

```
git archive --remote=git@github.com:mindslab-ai/import-binaries HEAD:master filename
```

## How to name the archive
- It should contain vendor, which means maker.
- It should contain unique name to distinguish other files.
- It should contain os, platform, framework informations. (android, win32, linux, macos, etc, arm, x86)
- It should contain version or date to distinguish other version.

- Use not '_' but '-'.

### examples
- etri-taapi-v0.1.tar.gz
